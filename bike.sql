-- MySQL dump 10.13  Distrib 5.7.24, for Linux (x86_64)
--
-- Host: localhost    Database: bike
-- ------------------------------------------------------
-- Server version	5.7.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can view log entry',1,'view_logentry'),(5,'Can add permission',2,'add_permission'),(6,'Can change permission',2,'change_permission'),(7,'Can delete permission',2,'delete_permission'),(8,'Can view permission',2,'view_permission'),(9,'Can add group',3,'add_group'),(10,'Can change group',3,'change_group'),(11,'Can delete group',3,'delete_group'),(12,'Can view group',3,'view_group'),(13,'Can add user',4,'add_user'),(14,'Can change user',4,'change_user'),(15,'Can delete user',4,'delete_user'),(16,'Can view user',4,'view_user'),(17,'Can add content type',5,'add_contenttype'),(18,'Can change content type',5,'change_contenttype'),(19,'Can delete content type',5,'delete_contenttype'),(20,'Can view content type',5,'view_contenttype'),(21,'Can add session',6,'add_session'),(22,'Can change session',6,'change_session'),(23,'Can delete session',6,'delete_session'),(24,'Can view session',6,'view_session'),(25,'Can add accesorio',7,'add_accesorio'),(26,'Can change accesorio',7,'change_accesorio'),(27,'Can delete accesorio',7,'delete_accesorio'),(28,'Can view accesorio',7,'view_accesorio'),(29,'Can add bike',8,'add_bike'),(30,'Can change bike',8,'change_bike'),(31,'Can delete bike',8,'delete_bike'),(32,'Can view bike',8,'view_bike'),(33,'Can add cliente',9,'add_cliente'),(34,'Can change cliente',9,'change_cliente'),(35,'Can delete cliente',9,'delete_cliente'),(36,'Can view cliente',9,'view_cliente'),(37,'Can add contrato',10,'add_contrato'),(38,'Can change contrato',10,'change_contrato'),(39,'Can delete contrato',10,'delete_contrato'),(40,'Can view contrato',10,'view_contrato'),(41,'Can add garantia',11,'add_garantia'),(42,'Can change garantia',11,'change_garantia'),(43,'Can delete garantia',11,'delete_garantia'),(44,'Can view garantia',11,'view_garantia'),(45,'Can add local',12,'add_local'),(46,'Can change local',12,'change_local'),(47,'Can delete local',12,'delete_local'),(48,'Can view local',12,'view_local'),(49,'Can add reserva',13,'add_reserva'),(50,'Can change reserva',13,'change_reserva'),(51,'Can delete reserva',13,'delete_reserva'),(52,'Can view reserva',13,'view_reserva'),(53,'Can add seguro',14,'add_seguro'),(54,'Can change seguro',14,'change_seguro'),(55,'Can delete seguro',14,'delete_seguro'),(56,'Can view seguro',14,'view_seguro'),(57,'Can add trabajador',15,'add_trabajador'),(58,'Can change trabajador',15,'change_trabajador'),(59,'Can delete trabajador',15,'delete_trabajador'),(60,'Can view trabajador',15,'view_trabajador');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'pbkdf2_sha256$120000$4cVm5pHidZqg$l1SC0NPUluvsqLIhoeHVG3Cmc41aTpj49iGtcOS5T5w=','2018-11-13 15:58:45.005508',1,'AlvaroAbarca','','','dead.time@live.cl',1,1,'2018-11-13 00:35:00.648708'),(2,'pbkdf2_sha256$120000$pLjNU1YJ0HNu$6Pc6od5HhA3KWcez8xZy+n9qffx+kNmUuwNtjHbQbww=','2018-11-13 02:06:33.313716',0,'IsraelCastro','','','',0,1,'2018-11-13 00:43:19.423568'),(3,'pbkdf2_sha256$120000$lNMm0nl0sZCO$cLYreqZNc786/UEs9gS4otnbWxXFk6rra/KhlS+y/WI=','2018-11-13 02:02:27.918380',0,'MarceloCavieres','','','',0,1,'2018-11-13 00:44:39.576227'),(4,'pbkdf2_sha256$120000$q7rMtRyJTjIL$RnAt8YEDSJLIVNp63SurfQ98T0m2fChzp26/d9aL+tk=',NULL,0,'NicolasCordova','','','',0,1,'2018-11-13 00:44:51.564938'),(5,'pbkdf2_sha256$120000$RWnt5JxNL4Yi$v8ptXWOfZacgvnwVI8PJdBOHPgJST8+UwpCWE1OPKN0=','2018-11-13 02:04:36.879870',0,'FranciscoFalcon','','','',0,1,'2018-11-13 00:45:04.598564'),(6,'pbkdf2_sha256$120000$O1YZlXqQ3GIy$3H8qJE3f0Q+jg5MKfsc56mmHdGoPzQ1HrGjreyh+fW4=','2018-11-13 23:34:58.328175',0,'GenesisGonzalez','','','',0,1,'2018-11-13 02:38:58.565833'),(7,'pbkdf2_sha256$120000$2olFkp7ZWxIs$gzXLNy1if6CsBT8ADQxlJbnJ/nfUk9A6IEFtCl8qkMU=','2018-11-13 23:30:53.915258',0,'JOrmeño','','','',0,1,'2018-11-13 02:40:41.512694'),(8,'pbkdf2_sha256$120000$GJES0ovgoTrV$vfl2JUEAmryzMRr5OC+dSC+Ei7MTXthLOKJ5DunEX3Y=','2018-11-14 17:32:11.252108',0,'FMahana','','','',0,1,'2018-11-13 02:43:10.611463');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_accesorio`
--

DROP TABLE IF EXISTS `core_accesorio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_accesorio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `created` datetime(6) NOT NULL,
  `last_updated` datetime(6) NOT NULL,
  `precio` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `core_accesorio_slug_b1761630` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_accesorio`
--

LOCK TABLES `core_accesorio` WRITE;
/*!40000 ALTER TABLE `core_accesorio` DISABLE KEYS */;
INSERT INTO `core_accesorio` VALUES (1,'Casco','casco','2018-11-13 00:40:43.278533','2018-11-13 00:40:43.278593',5000),(2,'Guantes','guantes','2018-11-13 00:40:59.908848','2018-11-13 00:40:59.908907',1000),(3,'Mochila','mochila','2018-11-13 00:41:14.323424','2018-11-13 00:41:14.323521',5000),(4,'Bombin','bombin','2018-11-13 00:41:21.721763','2018-11-13 00:41:21.721822',1000),(5,'Parches y Solución','parches-y-solucion','2018-11-13 00:41:28.329101','2018-11-13 00:41:28.329160',1000),(6,'Liquido Antipinchazo','liquido-antipinchazo','2018-11-13 00:41:41.666744','2018-11-13 00:41:41.666813',5000),(7,'Neumatico','neumatico','2018-11-13 00:41:53.360558','2018-11-13 00:41:53.360615',3000),(8,'Luz','luz','2018-11-13 00:42:03.434035','2018-11-13 00:42:03.434100',1000),(9,'Canasto','canasto','2018-11-13 00:42:14.303901','2018-11-13 00:42:14.303967',1000),(10,'Chaleco Reflectante','chaleco-reflectante','2018-11-13 00:42:22.058317','2018-11-13 00:42:22.058317',1000);
/*!40000 ALTER TABLE `core_accesorio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_bike`
--

DROP TABLE IF EXISTS `core_bike`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_bike` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(50) NOT NULL,
  `marca` varchar(30) NOT NULL,
  `modelo` varchar(50) NOT NULL,
  `precio` int(10) unsigned NOT NULL,
  `slug` varchar(50) NOT NULL,
  `created` datetime(6) NOT NULL,
  `last_updated` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `core_bike_slug_f3578360` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_bike`
--

LOCK TABLES `core_bike` WRITE;
/*!40000 ALTER TABLE `core_bike` DISABLE KEYS */;
INSERT INTO `core_bike` VALUES (1,'R','Perspi','P',67274,'perspi','2018-11-13 00:51:04.407402','2018-11-13 13:02:34.738828'),(2,'R','Repelle','P',23538,'repelle','2018-11-13 00:51:04.410972','2018-11-13 13:02:34.719637'),(3,'R','Necessitatibus repellendus nu','P',90920,'necessitatibus-repellendus-nu','2018-11-13 00:51:04.420676','2018-11-13 13:02:34.699634'),(4,'R','Alias totam doloremque il','D',31026,'alias-totam-doloremque-il','2018-11-13 00:51:04.425524','2018-11-13 13:02:34.684196'),(5,'D','Perspiciatis ducimu','P',68390,'perspiciatis-ducimu','2018-11-13 00:51:04.429070','2018-11-13 12:47:06.783090'),(6,'D','Earum facil','P',31274,'earum-facil','2018-11-13 00:51:04.432933','2018-11-13 03:13:19.460749'),(7,'D','Voluptate itaqu','P',25379,'voluptate-itaqu','2018-11-13 00:51:04.438006','2018-11-13 00:51:04.438063'),(8,'D','Tempor','P',93352,'tempor','2018-11-13 00:51:04.443446','2018-11-13 00:51:04.443509'),(9,'D','Numquam autem quo dolor','P',14136,'numquam-autem-quo-dolor','2018-11-13 00:51:04.447500','2018-11-13 00:51:04.447552'),(10,'D','Autem eligendi aliquam','P',68768,'autem-eligendi-aliquam','2018-11-13 00:51:04.451001','2018-11-13 00:51:04.451052'),(11,'D','Obcaecati nobis molestias,','D',98534,'obcaecati-nobis-molestias','2018-11-13 03:35:30.536611','2018-11-13 03:35:30.536676'),(12,'D','Autem eligendi aliquid volu','D',39557,'autem-eligendi-aliquid-volu','2018-11-13 03:35:30.540415','2018-11-13 03:35:30.540466'),(13,'D','Dolorum adipisci temporibus','P',73014,'dolorum-adipisci-temporibus','2018-11-13 03:35:30.544068','2018-11-13 03:35:30.544120'),(14,'D','Ad corporis at pariatur dolor','D',23486,'ad-corporis-at-pariatur-dolor','2018-11-13 03:35:30.555487','2018-11-13 03:35:30.555553'),(15,'D','Autem d','P',69971,'autem-d','2018-11-13 03:35:30.559988','2018-11-13 03:35:30.560052'),(16,'D','Ipsam qui','D',6910,'ipsam-qui','2018-11-13 03:35:30.564014','2018-11-13 03:35:30.564071'),(17,'D','A ipsa labor','D',60743,'a-ipsa-labor','2018-11-13 03:35:30.569063','2018-11-13 03:35:30.569120'),(18,'D','Distinctio vol','P',75928,'distinctio-vol','2018-11-13 03:35:30.572912','2018-11-13 03:35:30.572966'),(19,'D','Ex totam','P',38441,'ex-totam','2018-11-13 03:35:30.579305','2018-11-14 17:47:51.713298'),(20,'D','Recusandae sapient','P',445,'recusandae-sapient','2018-11-13 03:35:30.583524','2018-11-14 17:47:51.701512'),(21,'R','Amet quae cum mollitia quas,','D',36211,'amet-quae-cum-mollitia-quas','2018-11-13 03:35:30.587776','2018-11-14 02:21:51.286909'),(22,'R','Vit','D',68564,'vit','2018-11-13 03:35:30.591335','2018-11-14 02:21:51.265824'),(23,'R','Max','P',12597,'max','2018-11-13 03:35:30.595186','2018-11-14 02:21:51.250625'),(24,'R','Commodi quasi recusandae','D',31217,'commodi-quasi-recusandae','2018-11-13 03:35:30.600288','2018-11-14 02:17:00.700496'),(25,'R','Ess','P',83072,'ess','2018-11-13 03:35:30.604881','2018-11-14 02:17:00.679224'),(26,'R','Obc','P',57208,'obc','2018-11-13 03:35:30.609016','2018-11-14 02:17:00.661668'),(27,'R','Delectus deserunt culpa nece','P',72845,'delectus-deserunt-culpa-nece','2018-11-13 03:35:30.614000','2018-11-14 02:16:39.126769'),(28,'R','Delenit','D',22688,'delenit','2018-11-13 03:35:30.618214','2018-11-14 02:16:39.108502'),(29,'R','Nob','P',21301,'nob','2018-11-13 03:35:30.621808','2018-11-14 02:16:39.084674'),(30,'R','Sed consequuntur laudan','P',45368,'sed-consequuntur-laudan','2018-11-13 03:35:30.625463','2018-11-13 23:30:16.204986'),(31,'R','Est modi quos cupiditate','P',62200,'est-modi-quos-cupiditate','2018-11-13 03:35:30.629017','2018-11-13 23:26:57.951960'),(32,'R','Nihil quae','D',31301,'nihil-quae','2018-11-13 03:35:30.633913','2018-11-13 23:26:57.932362'),(33,'R','Laboriosam aut inventore qua','P',85757,'laboriosam-aut-inventore-qua','2018-11-13 03:35:30.638436','2018-11-13 23:26:57.914642'),(34,'R','Nisi quisquam dolores quasi','P',60708,'nisi-quisquam-dolores-quasi','2018-11-13 03:35:30.643229','2018-11-13 12:33:09.730629'),(35,'R','Ad quibusda','P',6107,'ad-quibusda','2018-11-13 03:35:30.646956','2018-11-13 19:38:47.174967'),(36,'R','Odit e','D',74100,'odit-e','2018-11-13 03:35:30.650078','2018-11-13 19:38:47.156217'),(37,'R','Beatae','P',22078,'beatae','2018-11-13 03:35:30.654086','2018-11-13 15:55:30.482480'),(38,'R','Commodi','D',16242,'commodi','2018-11-13 03:35:30.657509','2018-11-13 15:55:30.450539'),(39,'R','Blanditiis','D',67780,'blanditiis','2018-11-13 03:35:30.661411','2018-11-13 15:55:30.432958'),(40,'R','Saepe optio dese','D',32854,'saepe-optio-dese','2018-11-13 03:35:30.666949','2018-11-13 15:54:25.648958'),(41,'R','Qui','P',10642,'qui','2018-11-13 03:35:30.671324','2018-11-13 15:54:25.631311'),(42,'R','Magnam in necessitati','P',49788,'magnam-in-necessitati','2018-11-13 03:35:30.675634','2018-11-13 15:54:25.613391'),(43,'R','Adipisci rati','P',11298,'adipisci-rati','2018-11-13 03:35:30.679467','2018-11-13 15:52:15.111179'),(44,'R','Distinctio atque','P',61239,'distinctio-atque','2018-11-13 03:35:30.683043','2018-11-13 15:52:15.098524'),(45,'R','At amet tenetur laboriosam,','P',57170,'at-amet-tenetur-laboriosam','2018-11-13 03:35:30.687036','2018-11-13 15:52:15.082067'),(46,'R','Quis atque voluptatu','P',89338,'quis-atque-voluptatu','2018-11-13 03:35:30.690502','2018-11-13 15:07:51.390882'),(47,'R','Ex laudantium offic','P',54163,'ex-laudantium-offic','2018-11-13 03:35:30.694153','2018-11-13 15:07:51.370476'),(48,'R','Libero culpa qui','D',71758,'libero-culpa-qui','2018-11-13 03:35:30.698016','2018-11-13 15:07:51.352156'),(49,'R','Deleniti dignissimos','P',91839,'deleniti-dignissimos','2018-11-13 03:35:30.702518','2018-11-13 15:04:08.631230'),(50,'R','Iste','P',61365,'iste','2018-11-13 03:35:30.706663','2018-11-13 15:04:08.613685'),(51,'R','Ipsum molestias n','P',71849,'ipsum-molestias-n','2018-11-13 03:35:30.711304','2018-11-13 15:04:08.596035'),(52,'R','Mollitia recusandae ea','D',73927,'mollitia-recusandae-ea','2018-11-13 03:35:30.714947','2018-11-13 15:02:09.101588'),(53,'R','Magni mollitia at ratione','P',1440,'magni-mollitia-at-ratione','2018-11-13 03:35:30.718381','2018-11-13 15:02:09.080911'),(54,'R','Earum adi','P',7758,'earum-adi','2018-11-13 03:35:30.721922','2018-11-13 15:02:09.064036'),(55,'R','Inventore itaque in exc','P',26961,'inventore-itaque-in-exc','2018-11-13 03:35:30.725850','2018-11-13 13:36:20.008235'),(56,'R','Expedita','P',46141,'expedita','2018-11-13 03:35:30.729816','2018-11-13 13:36:19.979622'),(57,'R','Expedita consequatur invento','D',7617,'expedita-consequatur-invento','2018-11-13 03:35:30.734715','2018-11-13 19:38:47.141608'),(58,'R','Yaa<3','D',23322,'quibusd','2018-11-13 03:35:30.738377','2018-11-13 16:00:42.315177'),(59,'R','Copito el bambino','P',3678,'sequi-ali','2018-11-13 03:35:30.743151','2018-11-13 16:00:42.295604'),(60,'R','càsate conmigo','D',50057,'velit-nisi-nobis-maxime-im','2018-11-13 03:35:30.747472','2018-11-13 16:00:42.281678');
/*!40000 ALTER TABLE `core_bike` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_cliente`
--

DROP TABLE IF EXISTS `core_cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_cliente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `apellido` varchar(30) NOT NULL,
  `correo` varchar(254) NOT NULL,
  `telefono` varchar(17) NOT NULL,
  `rut` varchar(10) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `created` datetime(6) NOT NULL,
  `last_updated` datetime(6) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`),
  KEY `core_cliente_slug_1372bf37` (`slug`),
  CONSTRAINT `core_cliente_user_id_d7896daf_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_cliente`
--

LOCK TABLES `core_cliente` WRITE;
/*!40000 ALTER TABLE `core_cliente` DISABLE KEYS */;
INSERT INTO `core_cliente` VALUES (1,'Israel','Castro','israel.castro@mail.com','1231231239','18695756-3','israel','2018-11-13 00:47:42.114156','2018-11-13 00:47:42.114214',2),(2,'Francisco','Falcon','francisco.falcon@mail.com','1231231239','18626036-8','francisco','2018-11-13 00:48:45.906805','2018-11-13 00:48:45.906913',5),(3,'Genesis','Gonzalez','genesis.gonzalez.linda@gmail.com','+56952035626','20122086-6','genesis','2018-11-13 02:39:23.957760','2018-11-13 02:39:23.957843',6),(4,'Javiera','Ormeño','mail@test.com','+569123123123','19643899-8','javiera','2018-11-13 02:42:51.524432','2018-11-13 02:42:51.524488',7),(5,'Felipe','Mahana','mail@test.com','+56912345678','123456789-','felipe','2018-11-13 02:43:43.649938','2018-11-13 02:43:43.649995',8);
/*!40000 ALTER TABLE `core_cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_contrato`
--

DROP TABLE IF EXISTS `core_contrato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_contrato` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(50) NOT NULL,
  `created` datetime(6) NOT NULL,
  `last_updated` datetime(6) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `core_contrato_slug_13db1d47` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_contrato`
--

LOCK TABLES `core_contrato` WRITE;
/*!40000 ALTER TABLE `core_contrato` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_contrato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_garantia`
--

DROP TABLE IF EXISTS `core_garantia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_garantia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `total` int(10) unsigned NOT NULL,
  `cobrado` int(10) unsigned DEFAULT NULL,
  `observacion` longtext NOT NULL,
  `slug` varchar(50) NOT NULL,
  `created` datetime(6) NOT NULL,
  `last_updated` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `core_garantia_slug_d1f0e206` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_garantia`
--

LOCK TABLES `core_garantia` WRITE;
/*!40000 ALTER TABLE `core_garantia` DISABLE KEYS */;
INSERT INTO `core_garantia` VALUES (1,10000,0,'','10000','2018-11-13 00:53:00.151767','2018-11-13 00:53:00.162586'),(2,10000,0,'','10000-2','2018-11-13 02:52:57.860928','2018-11-13 02:52:57.866479'),(3,10000,0,'','10000-3','2018-11-13 03:09:29.276142','2018-11-13 03:09:29.280804'),(4,10000,0,'','10000-4','2018-11-13 03:14:24.901144','2018-11-13 03:14:24.908446'),(5,10000,0,'','10000-5','2018-11-13 03:15:34.439897','2018-11-13 03:15:34.445176'),(6,10000,0,'','10000-6','2018-11-13 03:23:59.953046','2018-11-13 03:23:59.963053'),(7,10000,0,'','10000-7','2018-11-13 12:32:03.383903','2018-11-13 12:32:03.389223'),(8,10000,0,'','10000-8','2018-11-13 12:33:09.674326','2018-11-13 12:33:09.687428'),(9,10000,0,'','10000-9','2018-11-13 13:02:04.824786','2018-11-13 13:02:04.829219'),(10,32000,0,'','10000-10','2018-11-13 13:02:34.605270','2018-11-13 13:02:34.837979'),(11,22000,0,'','10000-11','2018-11-13 13:36:19.910135','2018-11-13 13:36:20.096965'),(12,18000,0,'','10000-12','2018-11-13 15:02:09.035629','2018-11-13 15:02:09.178205'),(13,18000,0,'','10000-13','2018-11-13 15:04:08.555693','2018-11-13 15:04:08.734532'),(14,18000,0,'','10000-14','2018-11-13 15:07:51.311493','2018-11-13 15:07:51.483127'),(15,0,0,'','10000-15','2018-11-13 15:36:50.123659','2018-11-13 15:36:50.205112'),(16,18000,0,'','10000-16','2018-11-13 15:52:15.046186','2018-11-13 15:52:15.184386'),(17,0,0,'','10000-17','2018-11-13 15:54:08.980816','2018-11-13 15:54:09.039233'),(18,18000,0,'','10000-18','2018-11-13 15:54:25.566935','2018-11-13 15:54:25.751932'),(19,18000,0,'','10000-19','2018-11-13 15:55:30.366871','2018-11-13 15:55:30.568254'),(20,18000,0,'','10000-20','2018-11-13 16:00:42.263314','2018-11-13 16:00:42.390643'),(21,18000,0,'','10000-21','2018-11-13 19:38:47.114365','2018-11-13 19:38:47.243687'),(22,18000,0,'','10000-22','2018-11-13 23:26:57.880888','2018-11-13 23:26:58.036273'),(23,6000,0,'','10000-23','2018-11-13 23:30:16.179758','2018-11-14 01:11:24.067891'),(24,10000,0,'','10000-24','2018-11-14 02:03:00.699700','2018-11-14 02:03:00.706996'),(25,17000,0,'','10000-25','2018-11-14 02:16:39.044136','2018-11-14 02:16:39.204804'),(26,17000,0,'','10000-26','2018-11-14 02:17:00.611847','2018-11-14 02:17:00.783711'),(27,17000,0,'','10000-27','2018-11-14 02:21:51.211556','2018-11-14 02:21:51.385257'),(28,12000,0,'','10000-28','2018-11-14 17:47:11.832386','2018-11-14 17:47:11.931151');
/*!40000 ALTER TABLE `core_garantia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_local`
--

DROP TABLE IF EXISTS `core_local`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_local` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `ubicacion` varchar(100) NOT NULL,
  `telefono` int(11) NOT NULL,
  `sector` varchar(30) NOT NULL,
  `stock` int(11) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `created` datetime(6) NOT NULL,
  `last_updated` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `core_local_slug_b260180e` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_local`
--

LOCK TABLES `core_local` WRITE;
/*!40000 ALTER TABLE `core_local` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_local` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_reserva`
--

DROP TABLE IF EXISTS `core_reserva`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_reserva` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tiempo` int(10) unsigned NOT NULL,
  `garantia_id` int(11) NOT NULL,
  `status` varchar(50) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `created` datetime(6) NOT NULL,
  `last_updated` datetime(6) NOT NULL,
  `cliente_id` int(11) NOT NULL,
  `seguro_id` int(11) DEFAULT NULL,
  `token` varchar(100) NOT NULL,
  `fecha` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `seguro_id` (`seguro_id`),
  KEY `core_reserva_cliente_id_2ccfdd70_fk_core_cliente_id` (`cliente_id`),
  KEY `core_reserva_slug_30053ae9` (`slug`),
  KEY `core_reserva_garantia_id_e2328e0e` (`garantia_id`),
  CONSTRAINT `core_reserva_cliente_id_2ccfdd70_fk_core_cliente_id` FOREIGN KEY (`cliente_id`) REFERENCES `core_cliente` (`id`),
  CONSTRAINT `core_reserva_garantia_id_e2328e0e_fk_core_garantia_id` FOREIGN KEY (`garantia_id`) REFERENCES `core_garantia` (`id`),
  CONSTRAINT `core_reserva_seguro_id_8b9b4fc0_fk_core_seguro_id` FOREIGN KEY (`seguro_id`) REFERENCES `core_seguro` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_reserva`
--

LOCK TABLES `core_reserva` WRITE;
/*!40000 ALTER TABLE `core_reserva` DISABLE KEYS */;
INSERT INTO `core_reserva` VALUES (19,2,20,'I','i','2018-11-13 16:00:42.275320','2018-11-13 16:00:42.357396',3,8,'0','2018-11-13'),(22,2,23,'I','i-2','2018-11-13 23:30:16.191910','2018-11-13 23:30:16.232455',3,NULL,'0','2018-11-13'),(23,2,25,'I','i-3','2018-11-14 02:16:39.077865','2018-11-14 20:30:09.343525',5,NULL,'0','2018-11-14');
/*!40000 ALTER TABLE `core_reserva` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_reserva_accesorio`
--

DROP TABLE IF EXISTS `core_reserva_accesorio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_reserva_accesorio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reserva_id` int(11) NOT NULL,
  `accesorio_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `core_reserva_accesorio_reserva_id_accesorio_id_b7888fd2_uniq` (`reserva_id`,`accesorio_id`),
  KEY `core_reserva_accesor_accesorio_id_b10db8c6_fk_core_acce` (`accesorio_id`),
  CONSTRAINT `core_reserva_accesor_accesorio_id_b10db8c6_fk_core_acce` FOREIGN KEY (`accesorio_id`) REFERENCES `core_accesorio` (`id`),
  CONSTRAINT `core_reserva_accesorio_reserva_id_f2139b7e_fk_core_reserva_id` FOREIGN KEY (`reserva_id`) REFERENCES `core_reserva` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_reserva_accesorio`
--

LOCK TABLES `core_reserva_accesorio` WRITE;
/*!40000 ALTER TABLE `core_reserva_accesorio` DISABLE KEYS */;
INSERT INTO `core_reserva_accesorio` VALUES (36,19,8),(35,19,9),(34,19,10),(43,22,9),(45,23,9),(44,23,10);
/*!40000 ALTER TABLE `core_reserva_accesorio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_reserva_bike`
--

DROP TABLE IF EXISTS `core_reserva_bike`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_reserva_bike` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reserva_id` int(11) NOT NULL,
  `bike_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `core_reserva_bike_reserva_id_bike_id_b2366dac_uniq` (`reserva_id`,`bike_id`),
  KEY `core_reserva_bike_bike_id_7be11f5e_fk_core_bike_id` (`bike_id`),
  CONSTRAINT `core_reserva_bike_bike_id_7be11f5e_fk_core_bike_id` FOREIGN KEY (`bike_id`) REFERENCES `core_bike` (`id`),
  CONSTRAINT `core_reserva_bike_reserva_id_af3a7f77_fk_core_reserva_id` FOREIGN KEY (`reserva_id`) REFERENCES `core_reserva` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_reserva_bike`
--

LOCK TABLES `core_reserva_bike` WRITE;
/*!40000 ALTER TABLE `core_reserva_bike` DISABLE KEYS */;
INSERT INTO `core_reserva_bike` VALUES (47,19,58),(46,19,59),(45,19,60),(54,22,30),(57,23,27),(56,23,28),(55,23,29);
/*!40000 ALTER TABLE `core_reserva_bike` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_seguro`
--

DROP TABLE IF EXISTS `core_seguro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_seguro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(255) NOT NULL,
  `apellidos` varchar(255) NOT NULL,
  `rut` varchar(50) NOT NULL,
  `direccion` varchar(50) NOT NULL,
  `personas` smallint(5) unsigned NOT NULL,
  `tarjeta` varchar(50) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `created` datetime(6) NOT NULL,
  `last_updated` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `core_seguro_slug_7f9ec1d1` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_seguro`
--

LOCK TABLES `core_seguro` WRITE;
/*!40000 ALTER TABLE `core_seguro` DISABLE KEYS */;
INSERT INTO `core_seguro` VALUES (1,'Israel','Castro','18695756-3','Calle Falsa #123',2,'4051885600446620','18695756-3','2018-11-13 00:53:00.127268','2018-11-13 00:53:00.135846'),(2,'Javiera','Ormeño','19643899-8','Uno',2,'No','19643899-8','2018-11-13 02:52:57.847683','2018-11-13 02:52:57.853466'),(3,'Genesis','Gonzalez','20122086-6','asfdsfg',6,'4051885600446620','20122086-6','2018-11-13 03:09:29.255320','2018-11-13 03:09:29.260969'),(4,'Genesis','Gonzalez','20122086-6','asd',3,'4051885600446620','20122086-6-2','2018-11-13 03:14:24.878915','2018-11-13 03:14:24.887765'),(5,'Genesis','Gonzalez','20122086-6','asd',3,'4051885600446620','20122086-6-3','2018-11-13 03:15:34.414305','2018-11-13 03:15:34.418682'),(6,'Genesis','Gonzalez','20122086-6','',0,'','20122086-6-4','2018-11-13 15:36:50.070791','2018-11-13 15:36:50.075005'),(7,'Alvaro','Abarca','20122086-6','Roberto morande 2004',3,'','20122086-6-5','2018-11-13 15:52:15.002357','2018-11-13 15:52:15.009647'),(8,'Genesis','Gonzalez','20122086-6','123',3,'123','20122086-6-6','2018-11-13 16:00:42.210289','2018-11-13 16:00:42.213997');
/*!40000 ALTER TABLE `core_seguro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_trabajador`
--

DROP TABLE IF EXISTS `core_trabajador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_trabajador` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `rol` varchar(30) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `created` datetime(6) NOT NULL,
  `last_updated` datetime(6) NOT NULL,
  `local_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `core_trabajador_local_id_1e2938f8_fk_core_local_id` (`local_id`),
  KEY `core_trabajador_slug_a665c8da` (`slug`),
  CONSTRAINT `core_trabajador_local_id_1e2938f8_fk_core_local_id` FOREIGN KEY (`local_id`) REFERENCES `core_local` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_trabajador`
--

LOCK TABLES `core_trabajador` WRITE;
/*!40000 ALTER TABLE `core_trabajador` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_trabajador` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2018-11-13 00:40:43.279351','1','accesorio object (1)',1,'[{\"added\": {}}]',7,1),(2,'2018-11-13 00:40:59.914509','2','accesorio object (2)',1,'[{\"added\": {}}]',7,1),(3,'2018-11-13 00:41:14.324500','3','accesorio object (3)',1,'[{\"added\": {}}]',7,1),(4,'2018-11-13 00:41:21.725586','4','accesorio object (4)',1,'[{\"added\": {}}]',7,1),(5,'2018-11-13 00:41:28.330200','5','accesorio object (5)',1,'[{\"added\": {}}]',7,1),(6,'2018-11-13 00:41:41.670923','6','accesorio object (6)',1,'[{\"added\": {}}]',7,1),(7,'2018-11-13 00:41:53.361463','7','accesorio object (7)',1,'[{\"added\": {}}]',7,1),(8,'2018-11-13 00:42:03.438670','8','accesorio object (8)',1,'[{\"added\": {}}]',7,1),(9,'2018-11-13 00:42:14.304830','9','accesorio object (9)',1,'[{\"added\": {}}]',7,1),(10,'2018-11-13 00:42:22.062863','10','accesorio object (10)',1,'[{\"added\": {}}]',7,1),(11,'2018-11-13 00:43:19.662490','2','IsraelCastro',1,'[{\"added\": {}}]',4,1),(12,'2018-11-13 00:44:39.806808','3','MarceloCavieres',1,'[{\"added\": {}}]',4,1),(13,'2018-11-13 00:44:51.793288','4','NicolasCordova',1,'[{\"added\": {}}]',4,1),(14,'2018-11-13 00:45:04.824371','5','FranciscoFalcon',1,'[{\"added\": {}}]',4,1),(15,'2018-11-13 00:47:42.115130','1','cliente object (1)',1,'[{\"added\": {}}]',9,1),(16,'2018-11-13 00:48:45.913469','2','cliente object (2)',1,'[{\"added\": {}}]',9,1),(17,'2018-11-13 02:00:55.992437','1','reserva object (1)',2,'[{\"changed\": {\"fields\": [\"status\"]}}]',13,1),(18,'2018-11-13 02:33:13.998770','1','reserva object (1)',2,'[{\"changed\": {\"fields\": [\"seguro\"]}}]',13,1),(19,'2018-11-13 02:34:50.133153','1','reserva object (1)',2,'[{\"changed\": {\"fields\": [\"seguro\"]}}]',13,1),(20,'2018-11-13 02:34:50.190685','1','reserva object (1)',2,'[]',13,1),(21,'2018-11-13 02:38:58.814933','6','GenesisGonzalez',1,'[{\"added\": {}}]',4,1),(22,'2018-11-13 02:39:23.959021','3','cliente object (3)',1,'[{\"added\": {}}]',9,1),(23,'2018-11-13 02:40:41.746837','7','JOrmeño',1,'[{\"added\": {}}]',4,1),(24,'2018-11-13 02:42:51.530478','4','cliente object (4)',1,'[{\"added\": {}}]',9,1),(25,'2018-11-13 02:43:10.867771','8','FMahana',1,'[{\"added\": {}}]',4,1),(26,'2018-11-13 02:43:43.650952','5','cliente object (5)',1,'[{\"added\": {}}]',9,1),(27,'2018-11-13 03:13:09.882591','1','bike object (1)',2,'[{\"changed\": {\"fields\": [\"status\"]}}]',8,1),(28,'2018-11-13 03:13:19.462494','6','bike object (6)',2,'[{\"changed\": {\"fields\": [\"status\"]}}]',8,1),(29,'2018-11-13 03:22:27.019924','2','bike object (2)',2,'[{\"changed\": {\"fields\": [\"status\"]}}]',8,1),(30,'2018-11-13 03:22:31.564941','1','bike object (1)',2,'[{\"changed\": {\"fields\": [\"status\"]}}]',8,1),(31,'2018-11-13 03:39:39.908814','60','bike object (60)',2,'[{\"changed\": {\"fields\": [\"marca\"]}}]',8,1),(32,'2018-11-13 03:40:51.262487','59','bike object (59)',2,'[{\"changed\": {\"fields\": [\"marca\"]}}]',8,1),(33,'2018-11-13 12:46:26.191987','7','reserva object (7)',3,'',13,1),(34,'2018-11-13 12:46:26.196494','6','reserva object (6)',3,'',13,1),(35,'2018-11-13 12:46:26.200362','5','reserva object (5)',3,'',13,1),(36,'2018-11-13 12:46:26.203960','4','reserva object (4)',3,'',13,1),(37,'2018-11-13 12:46:26.207353','3','reserva object (3)',3,'',13,1),(38,'2018-11-13 12:46:26.216706','2','reserva object (2)',3,'',13,1),(39,'2018-11-13 12:46:26.219278','1','reserva object (1)',3,'',13,1),(40,'2018-11-13 12:46:46.382217','1','bike object (1)',2,'[{\"changed\": {\"fields\": [\"status\"]}}]',8,1),(41,'2018-11-13 12:46:57.024074','2','bike object (2)',2,'[{\"changed\": {\"fields\": [\"status\"]}}]',8,1),(42,'2018-11-13 12:47:06.785983','5','bike object (5)',2,'[{\"changed\": {\"fields\": [\"status\"]}}]',8,1),(43,'2018-11-13 12:47:17.474780','33','bike object (33)',2,'[{\"changed\": {\"fields\": [\"status\"]}}]',8,1),(44,'2018-11-13 15:59:00.955726','18','reserva object (18)',3,'',13,1),(45,'2018-11-13 15:59:00.960183','17','reserva object (17)',3,'',13,1),(46,'2018-11-13 15:59:00.964207','16','reserva object (16)',3,'',13,1),(47,'2018-11-13 15:59:00.968570','15','reserva object (15)',3,'',13,1),(48,'2018-11-13 15:59:00.971945','14','reserva object (14)',3,'',13,1),(49,'2018-11-13 15:59:00.975500','13','reserva object (13)',3,'',13,1),(50,'2018-11-13 15:59:00.985648','12','reserva object (12)',3,'',13,1),(51,'2018-11-13 15:59:00.989199','11','reserva object (11)',3,'',13,1),(52,'2018-11-13 15:59:00.992662','10','reserva object (10)',3,'',13,1),(53,'2018-11-13 15:59:00.996314','9','reserva object (9)',3,'',13,1),(54,'2018-11-13 15:59:01.000648','8','reserva object (8)',3,'',13,1),(55,'2018-11-13 15:59:29.277565','60','bike object (60)',2,'[{\"changed\": {\"fields\": [\"status\"]}}]',8,1),(56,'2018-11-13 15:59:38.319136','59','bike object (59)',2,'[{\"changed\": {\"fields\": [\"status\"]}}]',8,1),(57,'2018-11-13 15:59:44.365832','58','bike object (58)',2,'[{\"changed\": {\"fields\": [\"status\"]}}]',8,1),(58,'2018-11-13 15:59:51.449950','57','bike object (57)',2,'[{\"changed\": {\"fields\": [\"status\"]}}]',8,1);
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'admin','logentry'),(3,'auth','group'),(2,'auth','permission'),(4,'auth','user'),(5,'contenttypes','contenttype'),(7,'core','accesorio'),(8,'core','bike'),(9,'core','cliente'),(10,'core','contrato'),(11,'core','garantia'),(12,'core','local'),(13,'core','reserva'),(14,'core','seguro'),(15,'core','trabajador'),(6,'sessions','session');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2018-11-13 00:34:24.216875'),(2,'auth','0001_initial','2018-11-13 00:34:24.966915'),(3,'admin','0001_initial','2018-11-13 00:34:25.136447'),(4,'admin','0002_logentry_remove_auto_add','2018-11-13 00:34:25.158060'),(5,'admin','0003_logentry_add_action_flag_choices','2018-11-13 00:34:25.181747'),(6,'contenttypes','0002_remove_content_type_name','2018-11-13 00:34:25.290935'),(7,'auth','0002_alter_permission_name_max_length','2018-11-13 00:34:25.353837'),(8,'auth','0003_alter_user_email_max_length','2018-11-13 00:34:25.462684'),(9,'auth','0004_alter_user_username_opts','2018-11-13 00:34:25.491352'),(10,'auth','0005_alter_user_last_login_null','2018-11-13 00:34:25.554208'),(11,'auth','0006_require_contenttypes_0002','2018-11-13 00:34:25.560422'),(12,'auth','0007_alter_validators_add_error_messages','2018-11-13 00:34:25.580970'),(13,'auth','0008_alter_user_username_max_length','2018-11-13 00:34:25.646807'),(14,'auth','0009_alter_user_last_name_max_length','2018-11-13 00:34:25.711761'),(15,'core','0001_initial','2018-11-13 00:34:27.126939'),(16,'core','0002_cliente_user','2018-11-13 00:34:27.263574'),(17,'core','0003_auto_20181105_1316','2018-11-13 00:34:28.411929'),(18,'core','0004_auto_20181105_1748','2018-11-13 00:34:28.507955'),(19,'core','0005_auto_20181107_1536','2018-11-13 00:34:28.807404'),(20,'core','0006_auto_20181108_2337','2018-11-13 00:34:28.886292'),(21,'core','0007_auto_20181108_2339','2018-11-13 00:34:28.961228'),(22,'core','0008_auto_20181109_1442','2018-11-13 00:34:29.078073'),(23,'core','0009_auto_20181109_1453','2018-11-13 00:34:29.109234'),(24,'core','0010_auto_20181111_2148','2018-11-13 00:34:29.269128'),(25,'core','0011_auto_20181111_2149','2018-11-13 00:34:29.337011'),(26,'core','0012_auto_20181112_1700','2018-11-13 00:34:29.443846'),(27,'core','0013_reserva_token','2018-11-13 00:34:29.522795'),(28,'sessions','0001_initial','2018-11-13 00:34:29.576647'),(29,'core','0014_reserva_fecha','2018-11-14 02:10:00.781704');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('1543dbybxxf1qdd0tqiiiuwlhfchv0iq','MWNlM2RiYzAyMGM5ZGQ0OWM3MzllZWUwZmY3NzdlNGQyM2QwYTZiMjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIyOTRjNjgxNWNiNDE0Y2Q3NmU2OTJkZGI5ZDRhODg2NWMyNjY3ZDhhIn0=','2018-11-27 00:55:31.818699'),('5d8s3xna9p8e5657fzazfofnav3o8j4w','MWNlM2RiYzAyMGM5ZGQ0OWM3MzllZWUwZmY3NzdlNGQyM2QwYTZiMjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIyOTRjNjgxNWNiNDE0Y2Q3NmU2OTJkZGI5ZDRhODg2NWMyNjY3ZDhhIn0=','2018-11-27 12:34:50.767679'),('9r1noyv2gu5znql9zhc9tqc84s33l5gk','ZjE3NTlhZDcyNWY1ZWNmYTYyNzAxMjcwYTY0YjA3OWRhMTgxMmUxZDp7Il9hdXRoX3VzZXJfaWQiOiI3IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJjMzI3MTkzMzhkN2E0N2QyNDg3Yzc1OTNkZmU1ODgxNzFjNDhmMzM0In0=','2018-11-27 02:52:12.842960'),('q21lm9eap2r8aoh14d49gpgzpd8chbag','N2EyZDA0ZDhkYWEwMzk5ZDkzZTEwNzk2ZTRiNDQ3OGUyYjVjY2ZkYjp7Il9hdXRoX3VzZXJfaWQiOiI4IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJjMzAwN2YxZjNiZTA4M2M0M2VmMWM5YmM2MzQyZTE4YWQ5ZDFmNzRhIn0=','2018-11-28 17:32:11.258096'),('uu9bnjyufykvo37div6tf59mc5miljmr','ZTY0NDdhMzBlYzgyYThiNDI4NWVmMTM3MjQ1NDY5M2QyMTk0NjNmZTp7Il9hdXRoX3VzZXJfaWQiOiI2IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI0NDdkMTY5MTA4Yjc3ZDgyMzZlYTRiOGQ5NTFlMWU5NDY5ZjIwODQwIn0=','2018-11-27 03:22:02.031823'),('vil9tja6cy2ss7o062zippgspvb13k94','MWNlM2RiYzAyMGM5ZGQ0OWM3MzllZWUwZmY3NzdlNGQyM2QwYTZiMjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIyOTRjNjgxNWNiNDE0Y2Q3NmU2OTJkZGI5ZDRhODg2NWMyNjY3ZDhhIn0=','2018-11-27 15:58:45.012683');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-11-16 19:38:51
