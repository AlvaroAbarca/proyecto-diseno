# Proyecto Diseño

## Pre-requisitos

- Tener instalado python >= 3.6
- Mysql 5.7
- sudo apt-get install python3-dev python3-pip
- sudo python3 -m pip install virtualenv

### Mysql, OJO EN ESTE PASO

- sudo apt-get install mysql-server libmysqlclient-dev
- sudo mysql_install_db
- sudo mysql_secure_installation
- mysql -u root -p
- CREATE DATABASE bike CHARACTER SET UTF8;
- CREATE USER admin@localhost IDENTIFIED BY '1234';
- GRANT ALL PRIVILEGES ON bike.* TO admin@localhost;
- FLUSH PRIVILEGES;
- exit

### Pasos a seguir

1. Crear ambiente virtual
    - virtualenv -p python3 nombre
2. cd nombre
3. source bin/activate
4. git clone https://gitlab.com/AlvaroAbarca/proyecto-diseno
5. pip install -r requirements.txt
6. cd proyecto-diseno
7. python manage.py makemigrations
8. python manage.py migrate

### Referencias

- 
https://www.digitalocean.com/community/tutorials/how-to-use-mysql-or-mariadb-with-your-django-application-on-ubuntu-14-04
- https://www.digitalocean.com/community/tutorials/how-to-install-mysql-on-ubuntu-14-04