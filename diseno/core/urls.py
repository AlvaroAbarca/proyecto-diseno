from django.urls import path, include
from rest_framework import routers

from . import api
from . import views

router = routers.DefaultRouter()
router.register(r'local', api.localViewSet)
router.register(r'trabajador', api.trabajadorViewSet)
router.register(r'cliente', api.clienteViewSet)
router.register(r'accesorio', api.accesorioViewSet)
router.register(r'reserva', api.reservaViewSet)
router.register(r'garantia', api.garantiaViewSet)
router.register(r'contrato', api.contratoViewSet)
router.register(r'bike', api.bikeViewSet)
router.register(r'seguro', api.seguroViewSet)


urlpatterns = (
    # urls for Django Rest Framework API
    path('api/v1/', include(router.urls)),
    path('core/',views.inicio,name='inicio'),
    path('core/reserva1/', views.add_reserva, name='core_add_reserva'),
    path('core/reserva1/save/', views.save_reserva, name='core_save_reserva'),
    path('core/reserva1/final/', views.final_success, name='core_final_reserva'),
    path('core/reserva1/fail/', views.final_fail, name='core_fail_reserva'),
    
)
urlpatterns += (
    #path('', views.index, name="tbk_index"),
    #path('normal/', views.normal_index, name="normal"),
    path('core/init/', views.normal_init_transaction, name="normal_init"),
    path('core/return/', views.normal_return_from_webpay, name="normal_return"),
    #path('normal/final', views.normal_final, name="normal_final"),
)

urlpatterns += (
    # urls for local
    path('core/local/', views.localListView.as_view(), name='core_local_list'),
    path('core/local/create/', views.localCreateView.as_view(), name='core_local_create'),
    path('core/local/detail/<slug:slug>/', views.localDetailView.as_view(), name='core_local_detail'),
    path('core/local/update/<slug:slug>/', views.localUpdateView.as_view(), name='core_local_update'),
)

urlpatterns += (
    # urls for trabajador
    path('core/trabajador/', views.trabajadorListView.as_view(), name='core_trabajador_list'),
    path('core/trabajador/create/', views.trabajadorCreateView.as_view(), name='core_trabajador_create'),
    path('core/trabajador/detail/<slug:slug>/', views.trabajadorDetailView.as_view(), name='core_trabajador_detail'),
    path('core/trabajador/update/<slug:slug>/', views.trabajadorUpdateView.as_view(), name='core_trabajador_update'),
)

urlpatterns += (
    # urls for cliente
    path('core/cliente/', views.clienteListView.as_view(), name='core_cliente_list'),
    path('core/cliente/create/', views.clienteCreateView.as_view(), name='core_cliente_create'),
    path('core/cliente/detail/<slug:slug>/', views.clienteDetailView.as_view(), name='core_cliente_detail'),
    path('core/cliente/update/<slug:slug>/', views.clienteUpdateView.as_view(), name='core_cliente_update'),
)

urlpatterns += (
    # urls for accesorio
    path('core/accesorio/', views.accesorioListView.as_view(), name='core_accesorio_list'),
    path('core/accesorio/create/', views.accesorioCreateView.as_view(), name='core_accesorio_create'),
    path('core/accesorio/detail/<slug:slug>/', views.accesorioDetailView.as_view(), name='core_accesorio_detail'),
    path('core/accesorio/update/<slug:slug>/', views.accesorioUpdateView.as_view(), name='core_accesorio_update'),
)

urlpatterns += (
    # urls for reserva
    path('core/reserva/', views.reservaListView.as_view(), name='core_reserva_list'),
    path('core/reserva/create/', views.reservaCreateView.as_view(), name='core_reserva_create'),
    path('core/reserva/detail/<slug:slug>/', views.reservaDetailView.as_view(), name='core_reserva_detail'),
    path('core/reserva/update/<slug:slug>/', views.reservaUpdateView.as_view(), name='core_reserva_update'),
    path('core/reserva/delete/<slug:slug>/', views.reservaDeleteView.as_view(), name='core_reserva_delete'),
)

urlpatterns += (
    # urls for garantia
    path('core/garantia/', views.garantiaListView.as_view(), name='core_garantia_list'),
    path('core/garantia/create/', views.garantiaCreateView.as_view(), name='core_garantia_create'),
    path('core/garantia/detail/<slug:slug>/', views.garantiaDetailView.as_view(), name='core_garantia_detail'),
    path('core/garantia/update/<slug:slug>/', views.garantiaUpdateView.as_view(), name='core_garantia_update'),
)

urlpatterns += (
    # urls for contrato
    path('core/contrato/', views.contratoListView.as_view(), name='core_contrato_list'),
    path('core/contrato/create/', views.contratoCreateView.as_view(), name='core_contrato_create'),
    path('core/contrato/detail/<slug:slug>/', views.contratoDetailView.as_view(), name='core_contrato_detail'),
    path('core/contrato/update/<slug:slug>/', views.contratoUpdateView.as_view(), name='core_contrato_update'),
)

urlpatterns += (
    # urls for bike
    path('core/bike/', views.bikeListView.as_view(), name='core_bike_list'),
    path('core/bike/create/', views.bikeCreateView.as_view(), name='core_bike_create'),
    path('core/bike/detail/<slug:slug>/', views.bikeDetailView.as_view(), name='core_bike_detail'),
    path('core/bike/update/<slug:slug>/', views.bikeUpdateView.as_view(), name='core_bike_update'),
    path('core/bike/delete/<slug:slug>/', views.bikeDeleteView.as_view(), name='core_bike_delete'),
)

urlpatterns += (
    # urls for seguro
    path('core/seguro/', views.seguroListView.as_view(), name='core_seguro_list'),
    path('core/seguro/create/', views.seguroCreateView.as_view(), name='core_seguro_create'),
    path('core/seguro/detail/<slug:slug>/', views.seguroDetailView.as_view(), name='core_seguro_detail'),
    path('core/seguro/update/<slug:slug>/', views.seguroUpdateView.as_view(), name='core_seguro_update'),
)

