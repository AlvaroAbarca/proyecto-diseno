estado_bike = (
    ('D','Disponible'),
    ('R','Reservada'),
    ('M','Mantención'),
    ('B','Fuera de Servicio')
)
estado = (
    ('A','Activo'),
    ('I','Inactivo'),
    ('F','Finalizado')
)
estado_bike_default = 'D'
estado_default = 'A'
tipo_seguro = (
    ('C','Completo'),
    ('M','Medio'),
    ('B','Basico')
)
tipo_bike = (
    ('P','Paseo'),
    ('D','Deportiva')
)
tipo_tarjeta = (
    ('C','Credito'),
    ('D','Debito'),
)
tarjeta_default = 'C'