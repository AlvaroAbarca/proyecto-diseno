from django.urls import reverse
from django_extensions.db.fields import AutoSlugField
from django.db.models import *
from core.defines import *
from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.contrib.auth import models as auth_models
from django.db import models as models
from django_extensions.db import fields as extension_fields
from django.core.validators import RegexValidator


class local(models.Model):

    # Fields
    name = models.CharField(max_length=255)
    ubicacion = models.CharField(max_length=100)
    telefono = models.IntegerField()
    sector = models.CharField(max_length=30)
    stock = models.IntegerField()
    slug = extension_fields.AutoSlugField(populate_from='name', blank=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)


    class Meta:
        ordering = ('-created',)

    def __unicode__(self):
        return u'%s' % self.slug

    def get_absolute_url(self):
        return reverse('core_local_detail', args=(self.slug,))


    def get_update_url(self):
        return reverse('core_local_update', args=(self.slug,))


class trabajador(models.Model):

    # Fields
    name = models.CharField(max_length=255)
    rol = models.CharField(max_length=30)
    slug = extension_fields.AutoSlugField(populate_from='name', blank=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)

    # Relationship Fields
    local = models.ForeignKey(local,on_delete=models.CASCADE)

    class Meta:
        ordering = ('-created',)

    def __unicode__(self):
        return u'%s' % self.slug

    def get_absolute_url(self):
        return reverse('core_trabajador_detail', args=(self.slug,))


    def get_update_url(self):
        return reverse('core_trabajador_update', args=(self.slug,))


class cliente(models.Model):

    # Fields
    user = models.OneToOneField(User,null=True,on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    apellido = models.CharField(max_length=30)
    rut = models.CharField(max_length=10, help_text="123456789-0")
    correo = models.EmailField()
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    telefono = models.CharField(validators=[phone_regex], max_length=17, blank=True) # validators should be a list
    slug = extension_fields.AutoSlugField(populate_from='name', blank=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('-created',)

    def __unicode__(self):
        return u'%s' % self.slug

    def get_absolute_url(self):
        return reverse('core_cliente_detail', args=(self.slug,))

    def get_update_url(self):
        return reverse('core_cliente_update', args=(self.slug,))


    def full_name(self):
        return '{} {}' . format(self.name, self.apellido)



class accesorio(models.Model):

    # Fields
    name = models.CharField(max_length=255)
    precio = models.PositiveIntegerField()
    slug = extension_fields.AutoSlugField(populate_from='name', blank=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)


    class Meta:
        ordering = ('-created',)
    
    def __str__(self):
        return self.name

    def __unicode__(self):
        return u'%s' % self.slug

    def get_absolute_url(self):
        return reverse('core_accesorio_detail', args=(self.slug,))


    def get_update_url(self):
        return reverse('core_accesorio_update', args=(self.slug,))


class reserva(models.Model):

    # Fields
    tiempo = models.PositiveIntegerField() # Horas
    token = models.CharField(default = '0',blank=True,  max_length=100)
    fecha = models.DateField(auto_now=True, auto_now_add=False)
    status = models.CharField(max_length=50, choices = estado, default = estado_default)
    slug = extension_fields.AutoSlugField(populate_from='status', blank=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)

    # Relationship Fields
    cliente = models.ForeignKey(cliente,on_delete=models.CASCADE)
    accesorio = models.ManyToManyField('core.accesorio',verbose_name=("reserva_accesorios"))
    bike = models.ManyToManyField('core.bike', verbose_name=("reserva_bike"))
    garantia = models.ForeignKey('core.garantia', verbose_name=("garantia"), on_delete=models.CASCADE)
    #contrato = models.OneToOneField('core.contrato', verbose_name=("reserva_contrato"), on_delete=models.CASCADE)
    seguro = models.OneToOneField('core.seguro', verbose_name=("contrato_seguro"), on_delete=models.CASCADE, null= True,blank=True)
    
    
    def __str__(self):
        return self.slug

    class Meta:
        ordering = ('-created',)

    def __unicode__(self):
        return u'%s' % self.slug

    def get_absolute_url(self):
        return reverse('core_reserva_detail', args=(self.slug,))


    def get_update_url(self):
        return reverse('core_reserva_update', args=(self.slug,))

    def get_delete_url(self):
        return reverse('core_reserva_delete', args=(self.slug,))


class garantia(models.Model):

    # Fields
    total = models.PositiveIntegerField(default = 0)
    cobrado = models.PositiveIntegerField(default = 0, null=True)
    observacion = models.TextField(blank = True)
    slug = extension_fields.AutoSlugField(populate_from='total', blank=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)


    class Meta:
        ordering = ('-created',)
    
    def __str__(self):
        return self.slug

    def __unicode__(self):
        return u'%s' % self.slug

    def get_absolute_url(self):
        return reverse('core_garantia_detail', args=(self.slug,))


    def get_update_url(self):
        return reverse('core_garantia_update', args=(self.slug,))


class contrato(models.Model):

    # Fields
    name = models.CharField(max_length=50)
    slug = extension_fields.AutoSlugField(populate_from='name', blank=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)


    class Meta:
        ordering = ('-created',)

    def __unicode__(self):
        return u'%s' % self.slug

    def get_absolute_url(self):
        return reverse('core_contrato_detail', args=(self.slug,))


    def get_update_url(self):
        return reverse('core_contrato_update', args=(self.slug,))


class bike(models.Model):

    # Fields
    status = models.CharField(max_length=50, choices = estado_bike, default = estado_bike_default)
    marca = models.CharField(max_length=30)
    modelo = models.CharField(choices = tipo_bike,max_length=50)
    precio = models.PositiveIntegerField()
    slug = extension_fields.AutoSlugField(populate_from='marca', blank=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    
    
    def __str__(self):
        return self.marca

    class Meta:
        ordering = ('-created',)

    def __unicode__(self):
        return u'%s' % self.slug

    def get_absolute_url(self):
        return reverse('core_bike_detail', args=(self.slug,))


    def get_update_url(self):
        return reverse('core_bike_update', args=(self.slug,))
    
    def get_delete_url(self):
        return reverse('core_bike_delete', args=(self.slug,))


class seguro(models.Model):

    #Fields
    nombres = models.CharField(max_length=255)
    apellidos = models.CharField(max_length=255)
    rut = models.CharField(max_length=50)
    direccion = models.CharField(max_length=50)
    personas = models.PositiveSmallIntegerField()
    #tipo = models.CharField(choices = tipo_seguro, max_length=50)
    tarjeta = models.CharField(max_length=50, choices = tipo_tarjeta, default = tarjeta_default)
    slug = extension_fields.AutoSlugField(populate_from='rut', blank=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    
    
    def __str__(self):
        return self.nombres

    class Meta:
        ordering = ('-created',)

    def __unicode__(self):
        return u'%s' % self.slug

    def get_absolute_url(self):
        return reverse('core_bike_detail', args=(self.slug,))


    def get_update_url(self):
        return reverse('core_bike_update', args=(self.slug,))