import unittest
from django.urls import reverse
from django.test import Client
from .models import local, trabajador, cliente, accesorio, reserva, garantia, contrato, bike, seguro
from django.contrib.auth.models import User
from django.contrib.auth.models import Group
from django.contrib.contenttypes.models import ContentType


def create_django_contrib_auth_models_user(**kwargs):
    defaults = {}
    defaults["username"] = "username"
    defaults["email"] = "username@tempurl.com"
    defaults.update(**kwargs)
    return User.objects.create(**defaults)


def create_django_contrib_auth_models_group(**kwargs):
    defaults = {}
    defaults["name"] = "group"
    defaults.update(**kwargs)
    return Group.objects.create(**defaults)


def create_django_contrib_contenttypes_models_contenttype(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return ContentType.objects.create(**defaults)


def create_local(**kwargs):
    defaults = {}
    defaults["name"] = "name"
    defaults["ubicacion"] = "ubicacion"
    defaults["telefono"] = "telefono"
    defaults["sector"] = "sector"
    defaults["stock"] = "stock"
    defaults["slug"] = "slug"
    defaults.update(**kwargs)
    return local.objects.create(**defaults)


def create_trabajador(**kwargs):
    defaults = {}
    defaults["name"] = "name"
    defaults["rol"] = "rol"
    defaults["slug"] = "slug"
    defaults.update(**kwargs)
    if "local" not in defaults:
        defaults["local"] = create_local()
    return trabajador.objects.create(**defaults)


def create_cliente(**kwargs):
    defaults = {}
    defaults["name"] = "name"
    defaults["apellido"] = "apellido"
    defaults["correo"] = "correo"
    defaults["telefono"] = "telefono"
    defaults["rut"] = "rut"
    defaults["slug"] = "slug"
    defaults.update(**kwargs)
    return cliente.objects.create(**defaults)


def create_accesorio(**kwargs):
    defaults = {}
    defaults["name"] = "name"
    defaults["slug"] = "slug"
    defaults["precio"] = "precio"
    defaults.update(**kwargs)
    return accesorio.objects.create(**defaults)


def create_reserva(**kwargs):
    defaults = {}
    defaults["name"] = "name"
    defaults["garantia"] = "garantia"
    defaults["status"] = "status"
    defaults["slug"] = "slug"
    defaults.update(**kwargs)
    if "local" not in defaults:
        defaults["local"] = create_local()
    if "trabajador" not in defaults:
        defaults["trabajador"] = create_trabajador()
    if "cliente" not in defaults:
        defaults["cliente"] = create_cliente()
    if "accesorio" not in defaults:
        defaults["accesorio"] = create_accesorio()
    if "bike" not in defaults:
        defaults["bike"] = create_'core_bike'()
    return reserva.objects.create(**defaults)


def create_garantia(**kwargs):
    defaults = {}
    defaults["total"] = "total"
    defaults["cobrado"] = "cobrado"
    defaults["observacion"] = "observacion"
    defaults["slug"] = "slug"
    defaults.update(**kwargs)
    return garantia.objects.create(**defaults)


def create_contrato(**kwargs):
    defaults = {}
    defaults["slug"] = "slug"
    defaults.update(**kwargs)
    if "reserva" not in defaults:
        defaults["reserva"] = create_reserva()
    if "garantia" not in defaults:
        defaults["garantia"] = create_garantia()
    if "seguro" not in defaults:
        defaults["seguro"] = create_'core_seguro'()
    return contrato.objects.create(**defaults)


def create_bike(**kwargs):
    defaults = {}
    defaults["name"] = "name"
    defaults["status"] = "status"
    defaults["marca"] = "marca"
    defaults["modelo"] = "modelo"
    defaults["precio"] = "precio"
    defaults["slug"] = "slug"
    defaults.update(**kwargs)
    return bike.objects.create(**defaults)


def create_seguro(**kwargs):
    defaults = {}
    defaults["nombres"] = "nombres"
    defaults["apellidos"] = "apellidos"
    defaults["rut"] = "rut"
    defaults["direccion"] = "direccion"
    defaults["personas"] = "personas"
    defaults["tarjeta"] = "tarjeta"
    defaults["slug"] = "slug"
    defaults.update(**kwargs)
    return seguro.objects.create(**defaults)


class localViewTest(unittest.TestCase):
    '''
    Tests for local
    '''
    def setUp(self):
        self.client = Client()

    def test_list_local(self):
        url = reverse('diseno_local_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_local(self):
        url = reverse('diseno_local_create')
        data = {
            "name": "name",
            "ubicacion": "ubicacion",
            "telefono": "telefono",
            "sector": "sector",
            "stock": "stock",
            "slug": "slug",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_local(self):
        local = create_local()
        url = reverse('diseno_local_detail', args=[local.slug,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_local(self):
        local = create_local()
        data = {
            "name": "name",
            "ubicacion": "ubicacion",
            "telefono": "telefono",
            "sector": "sector",
            "stock": "stock",
            "slug": "slug",
        }
        url = reverse('diseno_local_update', args=[local.slug,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class trabajadorViewTest(unittest.TestCase):
    '''
    Tests for trabajador
    '''
    def setUp(self):
        self.client = Client()

    def test_list_trabajador(self):
        url = reverse('diseno_trabajador_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_trabajador(self):
        url = reverse('diseno_trabajador_create')
        data = {
            "name": "name",
            "rol": "rol",
            "slug": "slug",
            "local": create_local().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_trabajador(self):
        trabajador = create_trabajador()
        url = reverse('diseno_trabajador_detail', args=[trabajador.slug,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_trabajador(self):
        trabajador = create_trabajador()
        data = {
            "name": "name",
            "rol": "rol",
            "slug": "slug",
            "local": create_local().pk,
        }
        url = reverse('diseno_trabajador_update', args=[trabajador.slug,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class clienteViewTest(unittest.TestCase):
    '''
    Tests for cliente
    '''
    def setUp(self):
        self.client = Client()

    def test_list_cliente(self):
        url = reverse('diseno_cliente_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_cliente(self):
        url = reverse('diseno_cliente_create')
        data = {
            "name": "name",
            "apellido": "apellido",
            "correo": "correo",
            "telefono": "telefono",
            "rut": "rut",
            "slug": "slug",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_cliente(self):
        cliente = create_cliente()
        url = reverse('diseno_cliente_detail', args=[cliente.slug,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_cliente(self):
        cliente = create_cliente()
        data = {
            "name": "name",
            "apellido": "apellido",
            "correo": "correo",
            "telefono": "telefono",
            "rut": "rut",
            "slug": "slug",
        }
        url = reverse('diseno_cliente_update', args=[cliente.slug,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class accesorioViewTest(unittest.TestCase):
    '''
    Tests for accesorio
    '''
    def setUp(self):
        self.client = Client()

    def test_list_accesorio(self):
        url = reverse('diseno_accesorio_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_accesorio(self):
        url = reverse('diseno_accesorio_create')
        data = {
            "name": "name",
            "slug": "slug",
            "precio": "precio",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_accesorio(self):
        accesorio = create_accesorio()
        url = reverse('diseno_accesorio_detail', args=[accesorio.slug,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_accesorio(self):
        accesorio = create_accesorio()
        data = {
            "name": "name",
            "slug": "slug",
            "precio": "precio",
        }
        url = reverse('diseno_accesorio_update', args=[accesorio.slug,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class reservaViewTest(unittest.TestCase):
    '''
    Tests for reserva
    '''
    def setUp(self):
        self.client = Client()

    def test_list_reserva(self):
        url = reverse('diseno_reserva_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_reserva(self):
        url = reverse('diseno_reserva_create')
        data = {
            "name": "name",
            "garantia": "garantia",
            "status": "status",
            "slug": "slug",
            "local": create_local().pk,
            "trabajador": create_trabajador().pk,
            "cliente": create_cliente().pk,
            "accesorio": create_accesorio().pk,
            "bike": create_'core_bike'().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_reserva(self):
        reserva = create_reserva()
        url = reverse('diseno_reserva_detail', args=[reserva.slug,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_reserva(self):
        reserva = create_reserva()
        data = {
            "name": "name",
            "garantia": "garantia",
            "status": "status",
            "slug": "slug",
            "local": create_local().pk,
            "trabajador": create_trabajador().pk,
            "cliente": create_cliente().pk,
            "accesorio": create_accesorio().pk,
            "bike": create_'core_bike'().pk,
        }
        url = reverse('diseno_reserva_update', args=[reserva.slug,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class garantiaViewTest(unittest.TestCase):
    '''
    Tests for garantia
    '''
    def setUp(self):
        self.client = Client()

    def test_list_garantia(self):
        url = reverse('diseno_garantia_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_garantia(self):
        url = reverse('diseno_garantia_create')
        data = {
            "total": "total",
            "cobrado": "cobrado",
            "observacion": "observacion",
            "slug": "slug",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_garantia(self):
        garantia = create_garantia()
        url = reverse('diseno_garantia_detail', args=[garantia.slug,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_garantia(self):
        garantia = create_garantia()
        data = {
            "total": "total",
            "cobrado": "cobrado",
            "observacion": "observacion",
            "slug": "slug",
        }
        url = reverse('diseno_garantia_update', args=[garantia.slug,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class contratoViewTest(unittest.TestCase):
    '''
    Tests for contrato
    '''
    def setUp(self):
        self.client = Client()

    def test_list_contrato(self):
        url = reverse('diseno_contrato_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_contrato(self):
        url = reverse('diseno_contrato_create')
        data = {
            "slug": "slug",
            "reserva": create_reserva().pk,
            "garantia": create_garantia().pk,
            "seguro": create_'core_seguro'().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_contrato(self):
        contrato = create_contrato()
        url = reverse('diseno_contrato_detail', args=[contrato.slug,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_contrato(self):
        contrato = create_contrato()
        data = {
            "slug": "slug",
            "reserva": create_reserva().pk,
            "garantia": create_garantia().pk,
            "seguro": create_'core_seguro'().pk,
        }
        url = reverse('diseno_contrato_update', args=[contrato.slug,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class bikeViewTest(unittest.TestCase):
    '''
    Tests for bike
    '''
    def setUp(self):
        self.client = Client()

    def test_list_bike(self):
        url = reverse('diseno_bike_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_bike(self):
        url = reverse('diseno_bike_create')
        data = {
            "name": "name",
            "status": "status",
            "marca": "marca",
            "modelo": "modelo",
            "precio": "precio",
            "slug": "slug",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_bike(self):
        bike = create_bike()
        url = reverse('diseno_bike_detail', args=[bike.slug,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_bike(self):
        bike = create_bike()
        data = {
            "name": "name",
            "status": "status",
            "marca": "marca",
            "modelo": "modelo",
            "precio": "precio",
            "slug": "slug",
        }
        url = reverse('diseno_bike_update', args=[bike.slug,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class seguroViewTest(unittest.TestCase):
    '''
    Tests for seguro
    '''
    def setUp(self):
        self.client = Client()

    def test_list_seguro(self):
        url = reverse('diseno_seguro_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_seguro(self):
        url = reverse('diseno_seguro_create')
        data = {
            "nombres": "nombres",
            "apellidos": "apellidos",
            "rut": "rut",
            "direccion": "direccion",
            "personas": "personas",
            "tarjeta": "tarjeta",
            "slug": "slug",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_seguro(self):
        seguro = create_seguro()
        url = reverse('diseno_seguro_detail', args=[seguro.slug,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_seguro(self):
        seguro = create_seguro()
        data = {
            "nombres": "nombres",
            "apellidos": "apellidos",
            "rut": "rut",
            "direccion": "direccion",
            "personas": "personas",
            "tarjeta": "tarjeta",
            "slug": "slug",
        }
        url = reverse('diseno_seguro_update', args=[seguro.slug,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


