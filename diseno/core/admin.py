from django.contrib import admin
from django import forms
from .models import local, trabajador, cliente, accesorio, reserva, garantia, contrato, bike, seguro

class localAdminForm(forms.ModelForm):

    class Meta:
        model = local
        fields = '__all__'


class localAdmin(admin.ModelAdmin):
    form = localAdminForm
    list_display = ['name', 'ubicacion', 'telefono', 'sector', 'stock', 'slug', 'created', 'last_updated']
    readonly_fields = ['name', 'ubicacion', 'telefono', 'sector', 'stock', 'slug', 'created', 'last_updated']

admin.site.register(local, localAdmin)


class trabajadorAdminForm(forms.ModelForm):

    class Meta:
        model = trabajador
        fields = '__all__'


class trabajadorAdmin(admin.ModelAdmin):
    form = trabajadorAdminForm
    list_display = ['name', 'rol', 'slug', 'created', 'last_updated']
    readonly_fields = ['name', 'rol', 'slug', 'created', 'last_updated']

admin.site.register(trabajador, trabajadorAdmin)


class clienteAdminForm(forms.ModelForm):

    class Meta:
        model = cliente
        fields = '__all__'


class clienteAdmin(admin.ModelAdmin):
    form = clienteAdminForm
    list_display = ['name', 'apellido', 'rut', 'correo', 'telefono', 'slug', 'created', 'last_updated']
    #readonly_fields = ['name', 'apellido', 'correo', 'telefono', 'rut', 'slug', 'created', 'last_updated']

admin.site.register(cliente, clienteAdmin)


class accesorioAdminForm(forms.ModelForm):

    class Meta:
        model = accesorio
        fields = '__all__'


class accesorioAdmin(admin.ModelAdmin):
    form = accesorioAdminForm
    list_display = ['name', 'precio', 'slug', 'created', 'last_updated']
    #readonly_fields = ['name', 'precio', 'slug', 'created', 'last_updated']

admin.site.register(accesorio, accesorioAdmin)


class reservaAdminForm(forms.ModelForm):

    class Meta:
        model = reserva
        fields = '__all__'


class reservaAdmin(admin.ModelAdmin):
    form = reservaAdminForm
    list_display = ['tiempo', 'token', 'garantia', 'status', 'slug', 'created', 'last_updated']
    #readonly_fields = ['garantia', 'status', 'slug', 'created', 'last_updated']

admin.site.register(reserva, reservaAdmin)


class garantiaAdminForm(forms.ModelForm):

    class Meta:
        model = garantia
        fields = '__all__'


class garantiaAdmin(admin.ModelAdmin):
    form = garantiaAdminForm
    list_display = ['total', 'cobrado', 'observacion', 'slug', 'created', 'last_updated']
    readonly_fields = ['total', 'cobrado', 'observacion', 'slug', 'created', 'last_updated']

admin.site.register(garantia, garantiaAdmin)


class contratoAdminForm(forms.ModelForm):

    class Meta:
        model = contrato
        fields = '__all__'


class contratoAdmin(admin.ModelAdmin):
    form = contratoAdminForm
    list_display = ['slug', 'created', 'last_updated']
    readonly_fields = ['slug', 'created', 'last_updated']

admin.site.register(contrato, contratoAdmin)


class bikeAdminForm(forms.ModelForm):

    class Meta:
        model = bike
        fields = '__all__'


class bikeAdmin(admin.ModelAdmin):
    form = bikeAdminForm
    list_display = ['status', 'marca', 'modelo', 'precio', 'slug', 'created', 'last_updated']
    #readonly_fields = ['status', 'marca', 'modelo', 'precio', 'slug', 'created', 'last_updated']

admin.site.register(bike, bikeAdmin)


class seguroAdminForm(forms.ModelForm):

    class Meta:
        model = seguro
        fields = '__all__'


class seguroAdmin(admin.ModelAdmin):
    form = seguroAdminForm
    list_display = ['nombres', 'apellidos', 'rut', 'direccion', 'personas', 'tarjeta', 'slug', 'created', 'last_updated']
    readonly_fields = ['nombres', 'apellidos', 'rut', 'direccion', 'personas', 'tarjeta', 'slug', 'created', 'last_updated']

admin.site.register(seguro, seguroAdmin)


