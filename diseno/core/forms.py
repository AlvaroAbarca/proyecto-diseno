from django import forms
from .models import local, trabajador, cliente, accesorio, reserva, garantia, contrato, bike, seguro


class localForm(forms.ModelForm):
    class Meta:
        model = local
        fields = ['name', 'ubicacion', 'telefono', 'sector', 'stock']


class trabajadorForm(forms.ModelForm):
    class Meta:
        model = trabajador
        fields = ['name', 'rol', 'local']


class clienteForm(forms.ModelForm):
    class Meta:
        model = cliente
        fields = ['name', 'apellido', 'rut', 'correo', 'telefono']


class accesorioForm(forms.ModelForm):
    class Meta:
        model = accesorio
        fields = ['name', 'precio']


class reservaForm(forms.ModelForm):
    class Meta:
        model = reserva
        fields = ['tiempo', 'status', 'cliente', 'accesorio', 'bike','garantia','seguro']

class garantiaForm(forms.ModelForm):
    class Meta:
        model = garantia
        fields = ['total', 'cobrado', 'observacion']


class contratoForm(forms.ModelForm):
    class Meta:
        model = contrato
        fields = ['name']


class bikeForm(forms.ModelForm):
    class Meta:
        model = bike
        fields = ['status', 'marca', 'modelo', 'precio']


class seguroForm(forms.ModelForm):
    class Meta:
        model = seguro
        fields = ['nombres', 'apellidos', 'rut', 'direccion', 'personas', 'tarjeta']


