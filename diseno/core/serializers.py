from . import models

from rest_framework import serializers


class localSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.local
        fields = (
            'slug', 
            'name', 
            'ubicacion', 
            'telefono', 
            'sector', 
            'stock', 
            'created', 
            'last_updated', 
        )


class trabajadorSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.trabajador
        fields = (
            'slug', 
            'name', 
            'rol', 
            'created', 
            'last_updated', 
        )


class clienteSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.cliente
        fields = (
            'slug', 
            'name', 
            'apellido', 
            'correo', 
            'telefono', 
            'rut', 
            'created', 
            'last_updated', 
        )


class accesorioSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.accesorio
        fields = (
            'slug', 
            'name', 
            'created', 
            'last_updated', 
            'precio', 
        )


class reservaSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.reserva
        fields = (
            'slug', 
            'name', 
            'garantia', 
            'status', 
            'created', 
            'last_updated', 
        )


class garantiaSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.garantia
        fields = (
            'slug', 
            'total', 
            'cobrado', 
            'observacion', 
            'created', 
            'last_updated', 
        )


class contratoSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.contrato
        fields = (
            'slug', 
            'created', 
            'last_updated', 
        )


class bikeSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.bike
        fields = (
            'slug', 
            'name', 
            'status', 
            'marca', 
            'modelo', 
            'precio', 
            'created', 
            'last_updated', 
        )


class seguroSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.seguro
        fields = (
            'slug', 
            'nombres', 
            'apellidos', 
            'rut', 
            'direccion', 
            'personas', 
            'tarjeta', 
            'created', 
            'last_updated', 
        )


