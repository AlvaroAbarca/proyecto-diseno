from django.views.generic import DetailView, ListView, UpdateView, CreateView, DeleteView
from django.urls import reverse_lazy
from .models import local, trabajador, cliente, accesorio, reserva, garantia, contrato, bike, seguro
from .forms import localForm, trabajadorForm, clienteForm, accesorioForm, reservaForm, garantiaForm, contratoForm, bikeForm, seguroForm
from django.contrib.auth.models import User
from django.shortcuts import render
from django.contrib import messages
import os
from datetime import datetime
from django.http import JsonResponse,HttpResponseRedirect, HttpResponse
from django.db.models import Q
from django.contrib.auth.decorators import login_required
import tbk
from django.views.decorators.csrf import csrf_exempt

#### Transbank configuraciones 

CERTIFICATES_DIR = os.path.join(os.path.dirname(__file__), 'commerces')

def load_commerce_data(commerce_code):
    with open(os.path.join(CERTIFICATES_DIR, commerce_code, commerce_code + '.key'), 'r') as file:
        key_data = file.read()
    with open(os.path.join(CERTIFICATES_DIR, commerce_code, commerce_code + '.crt'), 'r') as file:
        cert_data = file.read()
    with open(os.path.join(CERTIFICATES_DIR, 'tbk.pem'), 'r') as file:
        tbk_cert_data = file.read()

    return {
        'key_data': key_data,
        'cert_data': cert_data,
        'tbk_cert_data': tbk_cert_data
    }

NORMAL_COMMERCE_CODE = "597020000541"
BASE_URL = 'http://localhost:8080'

normal_commerce_data = load_commerce_data(NORMAL_COMMERCE_CODE)
normal_commerce = tbk.commerce.Commerce(
    commerce_code=NORMAL_COMMERCE_CODE,
    key_data=normal_commerce_data['key_data'],
    cert_data=normal_commerce_data['cert_data'],
    tbk_cert_data=normal_commerce_data['tbk_cert_data'],
    environment=tbk.environments.DEVELOPMENT)
webpay_service = tbk.services.WebpayService(normal_commerce)

#### Transbank configuraciones 

#### Transbank vistas 
@login_required(login_url='/auth/login')
def normal_init_transaction(request):
    template_name = 'normal/init.html'
    data = {}
    if request.POST:
        transaction = webpay_service.init_transaction(
            amount=request.POST['amount'],
            buy_order=request.POST['buy_order'],
            return_url=BASE_URL + '/core/return/',
            final_url=BASE_URL + '/core/reserva1/final/',
            session_id=request.POST['csrfmiddlewaretoken']
            )
        data = {
            'transaction': transaction
        }
        return render(request,template_name,data)
    return render(request,template_name,data)

@csrf_exempt
def normal_return_from_webpay(request):
    template_name = 'core/final_success.html'
    template_name_fail = 'core/final_fail.html'
    data = {}
    if request.POST:
        token = request.POST['token_ws']
        transaction = webpay_service.get_transaction_result(token)
        transaction_detail = transaction['detailOutput'][0]
        rs = reserva.objects.get(pk=int(transaction_detail['buyOrder']))
        rs.token = token
        bikes = rs.bike.count()
        if transaction_detail['responseCode'] == 0:
            data = {
                    'id': rs.id,
                    'name': rs.cliente.name,
                    'apellido': rs.cliente.apellido,
                    'total': rs.garantia.total,
                    'n_bikes': bikes,
                    'n_accesorios': rs.accesorio.count(),
                    'transaction':transaction,
                    'transaction_detail':transaction_detail,
                    'token':token
                }
            rs.status = 'A'
            aux = rs.bike.all()
            for x in range(bikes):
                aux[x].status = 'R'
                aux[x].save()
            rs.save()
            return render(request,template_name,data)
        else:
            data = {
                'transaction':transaction,
                'transaction_detail':transaction_detail,
                'token':token
            }
            return render(request,template_name_fail,data)

#### Transbank configuraciones 


def inicio(request):
    data = {}
    template_name = 'index.html'
    return render(request,template_name,data)

@login_required(login_url='/auth/login')
def add_reserva(request):
    template_name = 'core/add_reserva.html'
    template_name_list = 'core/reserva_list1.html'
    data = {}
    r = reserva.objects.filter(status='A', cliente=request.user.cliente)
    data['accesorios_t'] = accesorio.objects.all()
    data['bikes_t'] = bike.objects.filter(Q(modelo='D', status='D') | Q(modelo='P',status='D'))

    if request.POST:
        print(request.POST)
        bikes_l = request.POST.getlist('bikes')
        if request.POST['seguro'] == 'si':
            s = seguro.objects.create(
                nombres = request.POST['nombres'],
                apellidos = request.POST['apellidos'],
                rut = request.POST['rut'],
                direccion = request.POST['direccion'],
                personas = len(bikes_l),
                tarjeta = request.POST['tarjeta'],
            )
            s.save()
        else:
            s = None
        g = garantia.objects.create(
            total = 10000,
            cobrado = 0,
            observacion = "",
        )
        status = 'I'
        c = request.user.cliente
        g.save()
        r = reserva.objects.create(
            tiempo = request.POST['tiempo'],
            fecha = request.POST['fecha'],
            status = status,
            cliente = c,
            garantia = g,
            seguro = s,
        )
        for x in bikes_l:
            bici = bike.objects.get(pk=x)
            r.bike.add(bici)
        accesorios_l = request.POST.getlist('accesorios')
        for s in accesorios_l:
            add = accesorio.objects.get(pk=s)
            r.accesorio.add(add)
        r.save()
        return HttpResponseRedirect('save/')
    if (r.count() == 0):
        return render(request,template_name,data)
    else:
        data['reserva'] = r.latest('created')
        data['bicis'] = data['reserva'].bike.count()
        return render(request,template_name_list,data)
def save_reserva(request):
    template_name = 'core/add_reserva_save.html'
    data = {}
    c = request.user.cliente
    res = reserva.objects.filter(cliente=c, status='I').latest('created')
    data['slug'] = res.id
    data['n_bikes'] = res.bike.count()
    data['n_accesorios'] = res.accesorio.count()
    res.garantia.total = (data['n_bikes'] * 5000) + (data['n_accesorios'] * 1000)
    res.garantia.save()
    data['total'] = res.garantia.total
    if res.seguro != None:
        data['seguro_e'] = 'Si'
    else:
        data['seguro_e'] = 'No'        
    return render(request,template_name,data)

def final_success(request):
    data = {}
    template_name = 'core/final_success.html'
    if request.POST:
        token = request.POST['token_ws']
        rs = reserva.objects.get(token)
    return render(request,template_name,data)

def final_fail(request):
    data = {}
    template_name = 'core/final_fail.html'
    if request.POST:
        res = reserva.objects.filter(cliente=request.user.cliente, status='A')
        res[0].status = 'I'
        token = token = request.POST['token_ws']
    return render(request,template_name,data)

class localListView(ListView):
    model = local


class localCreateView(CreateView):
    model = local
    form_class = localForm


class localDetailView(DetailView):
    model = local


class localUpdateView(UpdateView):
    model = local
    form_class = localForm


class trabajadorListView(ListView):
    model = trabajador


class trabajadorCreateView(CreateView):
    model = trabajador
    form_class = trabajadorForm


class trabajadorDetailView(DetailView):
    model = trabajador


class trabajadorUpdateView(UpdateView):
    model = trabajador
    form_class = trabajadorForm


class clienteListView(ListView):
    model = cliente


class clienteCreateView(CreateView):
    model = cliente
    form_class = clienteForm


class clienteDetailView(DetailView):
    model = cliente


class clienteUpdateView(UpdateView):
    model = cliente
    form_class = clienteForm


class accesorioListView(ListView):
    model = accesorio


class accesorioCreateView(CreateView):
    model = accesorio
    form_class = accesorioForm


class accesorioDetailView(DetailView):
    model = accesorio


class accesorioUpdateView(UpdateView):
    model = accesorio
    form_class = accesorioForm


class reservaListView(ListView):
    model = reserva


class reservaCreateView(CreateView):
    model = reserva
    form_class = reservaForm


class reservaDetailView(DetailView):
    model = reserva


class reservaUpdateView(UpdateView):
    model = reserva
    form_class = reservaForm

class reservaDeleteView(DeleteView):
    model = reserva
    success_url = reverse_lazy('inicio')


class garantiaListView(ListView):
    model = garantia


class garantiaCreateView(CreateView):
    model = garantia
    form_class = garantiaForm


class garantiaDetailView(DetailView):
    model = garantia


class garantiaUpdateView(UpdateView):
    model = garantia
    form_class = garantiaForm


class contratoListView(ListView):
    model = contrato


class contratoCreateView(CreateView):
    model = contrato
    form_class = contratoForm


class contratoDetailView(DetailView):
    model = contrato


class contratoUpdateView(UpdateView):
    model = contrato
    form_class = contratoForm


class bikeListView(ListView):
    model = bike
    context_object_name = 'bikes'
    paginate_by = 10
    queryset = bike.objects.all()


class bikeCreateView(CreateView):
    model = bike
    form_class = bikeForm


class bikeDetailView(DetailView):
    model = bike


class bikeUpdateView(UpdateView):
    model = bike
    form_class = bikeForm

class bikeDeleteView(DeleteView):
    model = bike
    success_url = reverse_lazy('core_bike_list')


class seguroListView(ListView):
    model = seguro


class seguroCreateView(CreateView):
    model = seguro
    form_class = seguroForm


class seguroDetailView(DetailView):
    model = seguro


class seguroUpdateView(UpdateView):
    model = seguro
    form_class = seguroForm

