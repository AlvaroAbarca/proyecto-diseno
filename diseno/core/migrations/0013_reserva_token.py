# Generated by Django 2.1.3 on 2018-11-12 21:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0012_auto_20181112_1700'),
    ]

    operations = [
        migrations.AddField(
            model_name='reserva',
            name='token',
            field=models.CharField(blank=True, default='0', max_length=100),
        ),
    ]
