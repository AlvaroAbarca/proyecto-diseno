# Generated by Django 2.1.3 on 2018-11-12 00:48

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0009_auto_20181109_1453'),
    ]

    operations = [
        migrations.AddField(
            model_name='reserva',
            name='qr_code',
            field=models.ImageField(default=1, upload_to='qr_code'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='reserva',
            name='garantia',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.garantia', verbose_name='garantia'),
        ),
    ]
