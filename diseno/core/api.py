from . import models
from . import serializers
from rest_framework import viewsets, permissions


class localViewSet(viewsets.ModelViewSet):
    """ViewSet for the local class"""

    queryset = models.local.objects.all()
    serializer_class = serializers.localSerializer
    permission_classes = [permissions.AllowAny]


class trabajadorViewSet(viewsets.ModelViewSet):
    """ViewSet for the trabajador class"""

    queryset = models.trabajador.objects.all()
    serializer_class = serializers.trabajadorSerializer
    permission_classes = [permissions.AllowAny]


class clienteViewSet(viewsets.ModelViewSet):
    """ViewSet for the cliente class"""

    queryset = models.cliente.objects.all()
    serializer_class = serializers.clienteSerializer
    permission_classes = [permissions.AllowAny]


class accesorioViewSet(viewsets.ModelViewSet):
    """ViewSet for the accesorio class"""

    queryset = models.accesorio.objects.all()
    serializer_class = serializers.accesorioSerializer
    permission_classes = [permissions.AllowAny]


class reservaViewSet(viewsets.ModelViewSet):
    """ViewSet for the reserva class"""

    queryset = models.reserva.objects.all()
    serializer_class = serializers.reservaSerializer
    permission_classes = [permissions.AllowAny]


class garantiaViewSet(viewsets.ModelViewSet):
    """ViewSet for the garantia class"""

    queryset = models.garantia.objects.all()
    serializer_class = serializers.garantiaSerializer
    permission_classes = [permissions.AllowAny]


class contratoViewSet(viewsets.ModelViewSet):
    """ViewSet for the contrato class"""

    queryset = models.contrato.objects.all()
    serializer_class = serializers.contratoSerializer
    permission_classes = [permissions.AllowAny]


class bikeViewSet(viewsets.ModelViewSet):
    """ViewSet for the bike class"""

    queryset = models.bike.objects.all()
    serializer_class = serializers.bikeSerializer
    permission_classes = [permissions.AllowAny]


class seguroViewSet(viewsets.ModelViewSet):
    """ViewSet for the seguro class"""

    queryset = models.seguro.objects.all()
    serializer_class = serializers.seguroSerializer
    permission_classes = [permissions.AllowAny]


