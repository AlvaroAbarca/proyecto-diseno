from django.apps import AppConfig


class BikeAuthConfig(AppConfig):
    name = 'bike_auth'
